function onClick() {
  let r = document.getElementById("result");
  r.innerHTML = "Тут должен быть ответ";
  let f1 = document.getElementsByName("field1");
  let f2 = document.getElementsByName("field2");
  if (!(/^[0-9]+$/).test(f1[0].value) || !(/^[0-9]+$/).test(f2[0].value)) {
      window.alert("Please only enter numeric characters!");
      return false;
  }
  r.innerHTML = "Cумма: " + (f1[0].value * f2[0].value) + " руб.";
  if (document.getElementById("defaultCheck1").checked) {
      r.innerHTML = "Cумма: " + ((f1[0].value * f2[0].value) + 100) + " руб.";
  }
  return false;
}

window.addEventListener("DOMContentLoaded", function () {
  //console.log("DOM fully loaded and parsed");
  let b = document.getElementById("button1");
  b.addEventListener("click", onClick);
  let r = document.getElementById("result");
  r.innerHTML = "Ответ";
});